import React from 'react';
import { Component } from 'react';
import s from './HomePage.module.scss'
import Header from '../../components/Header'
import Posts from '../../components/Posts'
import Footer from '../../components/Footer'

class HomePage extends Component {

  // constructor(props){
  //   super(props)


  render(props) {
    return (
      <div className={s.container}>
        <Header/>
        <Posts data={this.props.data}/>
        {/* <Footer/> */}
      </div>
    )
  }
}

export default HomePage
