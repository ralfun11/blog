import React from 'react';
import s from './Header.module.scss';

class Header extends React.Component {
  render() {
    return (
      <div className={s.container}>
        <div>
            <h1>Blog</h1>
        </div>

        <nav>
            <div id="home">
                <a>Home</a>
            </div>
            <div id="about">
                <a>About</a>
            </div>
        </nav>
      </div>
      )
  }
}

export default Header