import React, {useState} from 'react';
import s from './Posts.module.scss'
import Post from '../Post/'


const Posts = (props) => {
  const [posts, setPosts] = useState(
    props.data
  )

  return(
    <div className={s.container}>
        {posts.map(post => <Post key ={Math.random(15000)} post={post} ></Post>)}
    </div>
  )
}

export default Posts