import React from 'react';
import s from './Post.module.scss'


// const setSize = () => {
//     if (window.innerWidth <= 480) return s.xs
//     if (window.innerWidth < 640 && window.innerWidth >= 480) return s.s
//     if (window.innerWidth >= 640) return s.l
// }

const Post = (props) => {
    let { header, text, img } = props.post
    text = (text.length > 42) ? text.slice(0,200)+'...':text
    console.log(window.innerWidth);
    
    
return (
    <div className={s.container}>
        <h2>{header}</h2>
        <img src="https://picsum.photos/200/200" alt='Nothing to see here'></img>
        <p>{text}</p>
    </div>
    )
}
export default Post