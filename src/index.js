import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as app from "firebase/app";
import 'firebase/database'
import 'firebase/auth'

// app.initializeApp({
//     apiKey: "AIzaSyCmtBOHAQIPaIYFQq3zumnqdpkyLWn95yg",
//     authDomain: "blog-6221f.firebaseapp.com",
//     databaseURL: "https://blog-6221f.firebaseio.com",
//     projectId: "blog-6221f",
//     storageBucket: "blog-6221f.appspot.com",
//     messagingSenderId: "587262503389",
//     appId: "1:587262503389:web:1b0db3741ec02bbad66e34"
// })

app.initializeApp({
  apiKey: process.env.REACT_APP_API_KEY,
  authDomain: process.env.REACT_APP_AUTH_DOMAIN,
  databaseURL: process.env.REACT_APP_DATABASE_URL,
  projectId: process.env.REACT_APP_PROJECT_ID,
  storageBucket: process.env.REACT_APP_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_MESSAGING_SENDER_ID,
  appId: process.env.REACT_APP_APP_ID,
})

let userID = 'dfs023f'
const database = app.database()

let data = []
database.ref('/posts/').once('value').then(snapshot => {
  data = snapshot.val()
  console.log(data);
  ReactDOM.render(
    <React.StrictMode>
      <App data={data}/>
    </React.StrictMode>,
    document.getElementById('root')
  )
});