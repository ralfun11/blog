import React from 'react';
import './App.css';
import HomePage from './pages/HomePage'
import {BrowserRouter, Redirect, Route} from 'react-router-dom'

// function App() {
//   return (
//     <HomePage/>
//   );
// }
class App extends React.Component {

  render(){
    
    return (
              <BrowserRouter>
                <Route path='/' exact render={()=><HomePage data={this.props.data}/>}/>
                {/* <Route path='/home' render={()=><HomePage data={this.props.data}/>}/> */}
                {/* <Route path='/test' render={()=><h1>Something</h1>}/> */}
                {/* <Route   path='/login' component={LoginPage}/> */}
                {/*<Route render={()=><h1>Something went wrong</h1>}/>*/}
            </BrowserRouter>
    )
  }
}

export default App;
